/**
 * @file exception.s
 *
 * Copyright (c) 2014-2018 ARM Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 * Title:       Cortex-M Fault Exception handlers ( Common for both ARMv7M and ARMV6M )
 *
 * -----------------------------------------------------------------------------
 *
 *  Modified by Mark Giebler
 *	- allow return from fault handler to facilitate debugging.
 *  - add assert fault handler.  Generate via Software Intereupt 85. NVIC->STIR = 85;
 */
#ifndef MBED_FAULT_HANDLER_DISABLED

		.file    "exception.s"
		.syntax  unified

#ifndef __DOMAIN_NS
#define __DOMAIN_NS 1
#endif

		.equ     FAULT_TYPE_HARD_FAULT,         0xDEAD0010
		.equ     FAULT_TYPE_MEMMANAGE_FAULT,    0xDEAD0020
		.equ     FAULT_TYPE_BUS_FAULT,          0xDEAD0030
		.equ     FAULT_TYPE_USAGE_FAULT,        0xDEAD0040
//		.equ     FAULT_TYPE_ASSERT_FAULT,		0xDEAD0050	// define in arm_cortex_M4_fault_handler.c
//		.equ     FAULT_TYPE_NMI_FAULT,			0xDEAD0060  // define in arm_cortex_M4_fault_handler.c
		.thumb
		.section ".text"
		.align   2

// HardFault_Handler  is located in arm_cortex_M4_fault_handler.c

//HardFault_Handler
//        .thumb_func
//        .type    HardFault_Handler, %function
//        .global  HardFault_Handler
//        .fnstart
//        .cantunwind

//HardFault_Handler:
//        LDR      R3,=FAULT_TYPE_HARD_FAULT
//        B        Fault_Handler

//       .fnend
//        .size    HardFault_Handler, .-HardFault_Handler

// Assert_Handler  Moved to stm32l4xx_it.c
//		.thumb_func
//		.type    Assert_Handler, %function
//		.global  Assert_Handler
//		.fnstart
//		.cantunwind
//Assert_Handler:
//		LDR      R3,=FAULT_TYPE_ASSERT_FAULT
//		B        Fault_Handler

//		.fnend
//		.size    Assert_Handler, .-Assert_Handler


//Common Fault_Handler to capture the context
//
// called from various fault handler ISRs in arm_cortex_M4_fault_handler.c
// Stores ARM context into structure named: arm_fault_context declared in arm_cortex_M4_fault_handler.c
// Entry: R3 has fault type code.
// Stack layout assumed to be:
//	SP+0  :  R0
//	SP+4  :  R1
//	SP+8  :  R2
//	SP+12 :  R3
//	SP+16 :  R12
//	SP+20 :  LR
//	SP+24 :  PC
//	SP+28 :  xPSR
//
// Good info on ARM faults: https://interrupt.memfault.com/blog/cortex-m-fault-debug
//
		.thumb_func
		.type    Fault_Handler, %function
		.global  Fault_Handler
		.fnstart
		.cantunwind

Fault_Handler:
#if (__DOMAIN_NS == 1)
		MRS      R0,MSP
		LDR      R1,=0x4
		MOV      R2,LR
		TST      R2,R1                    // Check EXC_RETURN for bit 2
		BEQ      Fault_Handler_Continue
		MRS      R0,PSP

Fault_Handler_Continue:						// R0 has bottom of stack, R3 has fault type code
		MOV      R12,R3						// save fault type in R12
		ldr      R1,=arm_fault_context		//=arm_fault_type
		str      R12,[R1]
		ADDS     R1,#4		//		LDR      R1,=arm_fault_context		// get ctx structure address.
		LDR      R2,[R0]                  // Capture R0 from stack
		STR      R2,[R1]
		ADDS     R1,#4
		LDR      R2,[R0,#4]               // Capture R1 from stack
		STR      R2,[R1]
		ADDS     R1,#4
		LDR      R2,[R0,#8]               // Capture R2 from stack
		STR      R2,[R1]
		ADDS     R1,#4
		LDR      R2,[R0,#12]              // Capture R3 from stack
		STR      R2,[R1]
		ADDS     R1,#4
		STMIA    R1!,{R4-R7}              // Capture R4..R7 directly
		MOV      R7,R8                    // Capture R8
		STR      R7,[R1]
		ADDS     R1,#4
		MOV      R7,R9                    // Capture R9
		STR      R7,[R1]
		ADDS     R1,#4
		MOV      R7,R10                   // Capture R10
		STR      R7,[R1]
		ADDS     R1,#4
		MOV      R7,R11                   // Capture R11
		STR      R7,[R1]
		ADDS     R1,#4
		LDR      R2,[R0,#16]              // Capture R12 from stack
		STR      R2,[R1]
		ADDS     R1,#8                    // Add 8 here to capture LR next, we will capture SP later
		LDR      R2,[R0,#20]              // Capture LR from stack
		STR      R2,[R1]
		ADDS     R1,#4
		LDR      R2,[R0,#24]              // Capture PC from stack
		STR      R2,[R1]
		ADDS     R1,#4
		LDR      R2,[R0,#28]              // Capture xPSR from stack
		STR      R2,[R1]
		ADDS     R1,#4
		// Adjust stack pointer to its original value and capture it
		MOV      R3,R0
		ADDS     R3,#0x20                 // Add 0x20 to get the SP value prior to exception
		LDR      R6,=0x200
		TST      R2,R6                    // Check for if STK was aligned by checking bit-9 in xPSR value
		BEQ      Fault_Handler_Continue1
		ADDS     R3,#0x4

Fault_Handler_Continue1:
		MOV      R5,LR
		LDR      R6,=0x10                 // Check for bit-4 to see if FP context was saved
		TST      R5,R6
		BNE      Fault_Handler_Continue2
		ADDS     R3,#0x48                 // 16 FP regs + FPCSR + 1 Reserved

Fault_Handler_Continue2:
		MOV      R4,R1
		SUBS     R4,#0x10                 // Set the location of SP in ctx structure
		STR      R3,[R4]                  // Capture the adjusted SP
		MRS      R2,PSP                   // Get PSP
		STR      R2,[R1]
		ADDS     R1,#4
		MRS      R2,MSP                   // Get MSP
		STR      R2,[R1]
		ADDS     R1,#4
		MOV      R2,LR                    // Get current LR(EXC_RETURN)
		STR      R2,[R1]
		ADDS     R1,#4
		MRS      R2,CONTROL               // Get CONTROL Reg
		STR      R2,[R1]
		// _mg_ get other registers of interest.
		ADDS     R1,#4
		// System Handler Control and State Register
		LDR      R0,=0xE000ED24			// SHCSR 0xE000ED24
		LDR      R2,[R0]
		STR      R2,[R1]
		ADDS     R1,#4
		// Configurable Fault Status Registers (CFSR) - 0xE000ED28 (32 bit)
		//		https://interrupt.memfault.com/blog/cortex-m-fault-debug#cfsr
		// Bits 31 - 16  USFR - UsageFault Status Register (UFSR) - 0xE000ED2A
		//				25 - DivdeByZero
		//				24 - Unaligned
		//				19 - NOCP - coprocessor disabed when float operation attempeted.
		//				18 - INVPC
		//				17 - INVSTATE -  execute an instruction with an invalid Execution Program Status Register (EPSR) value.
		//				16 - UNDEFINSTR
		// Bits 15 - 8   BFSR - BusFault Status Register (BFSR) - 0xE002ED29
		//				15 - BFARVALID  - BFAR has address offault
		//				14 - reserved
		//				13 - LSPERR  stack address invalid can cause this
		//				12 - STKERR  stack address invalid can cause this
		//				11 - UNSTKERR corrupt stack or invalid address
		//				10 - IMPRECISERR fault address may not be accurate due to pipelined write to memory. Look for bad write address.
		//				 9 - PRECISERR fault address is accurate.
		//				 8 - IBUSERR
		// Bits 7  - 0   MMFSR -MemManage Status Register (MMFSR) - 0xE000ED28
		//				 7 - MMARVALID - MMFAR has valid fault address
		//				 6 - reserved
		//				 5 - MLSPERR  invalid stack address
		//				 4 - MSTKERR  invalid stack address
		//				 3 - MUNSTKERR
		//				 2 - fault while returning from exception
		//				 1 - DACCVIOL data access triggered the memory fault
		//				 0 - IACCVIOL instruction execution attempt triggered fault
		//
		ADDS     R0,#4					// CFSR 0xE000ED28
		LDR      R2,[R0]
		STR      R2,[R1]
		ADDS     R1,#4
		// HardFault Status Register (HFSR) - 0xE000ED2C
		// Bits 31 - 0
		//				31 - DEBUGEVT break point hit while not debugging
		//				30 - FORCED - fault handler had a fault.
		//				 1 - VECTTBL - bad address from the vector table (e.g. rouge interrupt with not valid entry in table)
		//				 0 - reserved
		ADDS     R0,#4					// HFSR 0xE000ED2C
		LDR      R2,[R0]
		STR      R2,[R1]
		ADDS     R1,#4
		// Debug Fault Status Register
		ADDS     R0,#4					// DFSR 0xE000ED30
		LDR      R2,[R0]
		STR      R2,[R1]
		ADDS     R1,#4

		// Read the Fault Address Registers. These may not contain valid values.
		// Check BFARVALID/MMARVALID to see if they are valid values
		// MemManage Fault Address Register
		ADDS     R0,#4					// MMFAR 0xE000ED34
		LDR      R2,[R0]
		STR      R2,[R1]
		ADDS     R1,#4
		// Bus Fault Address Register
		ADDS     R0,#4					// BFAR 0xE000ED38
		LDR      R2,[R0]
		STR      R2,[R1]
		ADDS     R1,#4

		// Auxiliary Fault Status Register
		ADDS     R0,#0x4				// AFSR 0xE000ED3C
		LDR      R2,[R0]
		STR      R2,[R1]

		// use below for mbed OS way of printing out data.
//		LDR      R3,=arm_fault_handler   // Load address of armFaultHandler
//		MOV      R0,R12					 // fault type (R12) to first parameter.
//		LDR      R1,=arm_fault_context
//		LDR      R2,=osRtxInfo
//		BLX      R3
//		BX LR							// return to caller (when debugging)

		// branch to C code for further handling in file stm32l4xx_it.c
		B        arm_fault_handler

#endif
		B        .                      // Just in case we come back here

		.fnend
		.size    Fault_Handler, .-Fault_Handler

#endif

		.end


