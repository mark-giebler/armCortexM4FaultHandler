# ARM Cortex M4 Fault Handler

ARM fault handler that captures registers after various types of faults and assert() events.
Designed to replace the default handlers in the STM32 HAL by integrating
my modified version of the MbedOS fault handler.

See code comments for details on how to integrate into a STM32 HAL based project.

This handler works with other Cortex ARM cores as well as M4s (e.g. M0, M3, M7, etc.).

Derived from these sources:
 - [https://www.freertos.org/Debugging-Hard-Faults-On-Cortex-M-Microcontrollers.html](https://www.freertos.org/Debugging-Hard-Faults-On-Cortex-M-Microcontrollers.html)
 - Further enhanced by using the [mbed OS version.](https://k1.spdns.de/Develop/Hardware/STM32/mbed/mbed-os-example-blinky/mbed-os/rtos/TARGET_CORTEX/TARGET_CORTEX_M/mbed_rtx_fault_handler.c)
 - For capturing more fault details, [I referenced this code.](http://www.chibios.com/forum/viewtopic.php?t=743)

Combined in 2018 by **Mark Giebler** for some home projects (including an Atari 2600 cartridge emulator).


## GCC Linker Configuration

To persist the ARM fault context data across resets,
I put the ARM fault context data into the **.noinit.armFaultData** section of RAM.
This is handy if you have a watchdog timer that resets the system automatically after a crash.
The data can still be dumped out after the reset.

The following is an example snippet of a gcc linker script file to create this section.

```
	.noinit (NOLOAD) :
	{

	_snoinit = .;
	__noinit_start__ = _snoinit;
	/* noinit sections */
	 *(.noinit.bootloaderData)
	. = ALIGN(256);	/* 0x0000000010000500 */
	 *(.noinit.armFaultData)
	 *(.noinit)	/* remaining noinit data */

	}>RAM

```


## Crash Log Parser Tool

**crash_log_parser.py** is a post-processing tool that can be used to parse crash logs generated
by the ARM fault handler code when an exception happens and the stored context is output via the serial port.

>Note: By default, **crash_log_parser.py** requires the arm-gcc binary **arm-none-eabi-nm**
be in the current environment PATH.
**I added an optional command line argument** to allow specifying the path to the binary.
This helps in the case you have many different build environments with different arm-gcc versions like me.

My modified [**crash_log_parser.py** is here.](crash_log_parser.py)

The original Python fault dump parser (without my modification) is
from here [mbed fault dump parser.](https://github.com/ARMmbed/mbed-os/tree/master/tools/debug_tools/crash_log_parser)

Another link to the [fault parser.](https://github.com/ARMmbed/mbed-os-example-fault-handler)

## Capturing crash log

When an exception happens the ARM Fault Handler will capture the context of the fault into a
**.noinit** section of RAM. Being in the **.noinit** section allows the saved context to not be zero filled when
the system is reset. The saved fault context can be dumped via a serial port
by calling the function **dump_arm_fault()**.

This serial output can be captured to a log file using a serial tool.
A tool such as my [serial monitor.](https://gitlab.com/mark-giebler/Marks-Monitor-App)

The crash information contains register context at the time of the exception.
The information printed out to the serial port will be similar to below. Registers captured depends on specific
Cortex-M core you are using. For example, if your target is using Cortex-M0, some registers like
MMFSR, BFSR, UFSR may not be available and will not appear in the crash log.

```
++ ARM Fault Handler ++

FaultType: HardFault

Context:
R0   : 0000AAA3
R1   : 20002070
R2   : 00009558
R3   : 00412A02
R4   : E000ED14
R5   : 00000000
R6   : 00000000
R7   : 00000000
R8   : 00000000
R9   : 00000000
R10  : 00000000
R11  : 00000000
R12  : 0000BCE5
SP   : 20002070
LR   : 00009E75
PC   : 00009512
xPSR : 01000000
PSP  : 20002008
MSP  : 2002FFD8
CPUID: 410FC241
HFSR : 40000000
MMFSR: 00000000
BFSR : 00000000
UFSR : 00000100
DFSR : 00000008
AFSR : 00000000
SHCSR: 00000000

-- ARM Fault Handler --
```

To generate more information save this crash information to a text file and
run the [**crash_log_parser.py**](crash_log_parser.py) tool as shown below.

>NOTE: Make sure you copy the section including the text "++ ARM Fault Handler ++" as the tool
looks for that header.

## Running the Crash Log Parser

I modified the original parser to add an optional 4th command line argument to
allow specifying the path to the **arm-none-eabi-nm** binary.
This helps in the case you have many different build environments with different arm-gcc versions like me.
If not specified it defaults to look in the same directory it was launched in.

`crash_log_parser.py <Path to Crash log> <Path to Elf/Axf file of the build> <Path to Map file of the build> [<path-to-arm-gcc-nm>]`

For example assuming **arm-none-eabi-nm** is in the same directory as **crash_log_parser.py**:

```
crash_log_parse.py ./crash.log ~/workspace/project/my_app.elf ~/workspace/project/my_app.map
```

An example output from running **crash_log_parser.py** is shown below.

```
Parsed Crash Info:
        Crash location = zero_div_test() [0000693E]
        Caller location = $Super$$main [00009E99]
        Stack Pointer at the time of crash = [20001CC0]
        Target/Fault Info:
                Processor Arch: ARM-V7M or above
                Processor Variant: C24
                Forced exception, a fault with configurable priority has been escalated to HardFault
                Divide by zero error has occurred

Done parsing...
```


# License Details

Licensed under the Apache License, Version 2.0 (the License); you may
not use this file except in compliance with the License.
You may obtain a copy of the License at

	www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an AS IS BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
